package qa.automation.com.coposit.DTO;

public class AdminLoginDTO {
	
	String email;
	String password;
	
	public AdminLoginDTO(String email, String password) {
		super();
		this.email = email;
		this.password = password;
	}

	public String getemail() {
		return email;
	}

	public void setemail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}	

}
