package qa.automation.com.coposit.DTO;

public class MakeEnquiryDTO {
	
	double depositPercentage;
	double propertyPrice;
	String noOfBedroom;
	String suburb;
	String message; 
	boolean requestCall; 
	boolean scheduleInspection; 
	double additionalUpfrontDeposit;
	String propertyId;
	int numberOfPayment;
	
	
	public MakeEnquiryDTO(double depositPercentage, double propertyPrice, String noOfBedroom, String suburb,
			String message, boolean requestCall, boolean scheduleInspection, double additionalUpfrontDeposit, String propertyId, int numberOfPayment) {
		super();
		this.depositPercentage = depositPercentage;
		this.propertyPrice = propertyPrice;
		this.noOfBedroom = noOfBedroom;
		this.suburb = suburb;
		this.message = message;
		this.requestCall = requestCall;
		this.scheduleInspection = scheduleInspection;
		this.additionalUpfrontDeposit = additionalUpfrontDeposit;
		this.propertyId = propertyId;
		this.numberOfPayment = numberOfPayment;
	}

	public double getDepositPercentage() {
		return depositPercentage;
	}

	public void setDepositPercentage(double depositPercentage) {
		this.depositPercentage = depositPercentage;
	}

	public double getPropertyPrice() {
		return propertyPrice;
	}

	public void setPropertyPrice(double propertyPrice) {
		this.propertyPrice = propertyPrice;
	}

	public String getNoOfBedroom() {
		return noOfBedroom;
	}

	public void setNoOfBedroom(String noOfBedroom) {
		this.noOfBedroom = noOfBedroom;
	}

	public String getSuburb() {
		return suburb;
	}

	public void setSuburb(String suburb) {
		this.suburb = suburb;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isRequestCall() {
		return requestCall;
	}

	public void setRequestCall(boolean requestCall) {
		this.requestCall = requestCall;
	}

	public boolean isScheduleInspection() {
		return scheduleInspection;
	}

	public void setScheduleInspection(boolean scheduleInspection) {
		this.scheduleInspection = scheduleInspection;
	}

	public double getAdditionalUpfrontDeposit() {
		return additionalUpfrontDeposit;
	}

	public void setAdditionalUpfrontDeposit(double additionalUpfrontDeposit) {
		this.additionalUpfrontDeposit = additionalUpfrontDeposit;
	}
	
	public String getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(String propertyId) {
		this.propertyId = propertyId;
	}
	
	public int getNumberOfPayment() {
		return numberOfPayment;
	}

	public void setNumberOfPayment(int numberOfPayment) {
		this.numberOfPayment = numberOfPayment;
	}

}
