package qa.automation.com.coposit.Exception;

public class IncorrectURL extends Exception {
	public IncorrectURL(String message) {
		super(message);
	}
}
