package qa.automation.com.coposit.Listeners;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;


import com.relevantcodes.extentreports.ExtentReports;

import qa.automation.com.coposit.utils.Helper;

/**
 * 
 * Date :- 13/07/2019
 * This is ExtentManager class that has path where the extent report to be generated.
 * @author Basanta
 * @version 1.0
 *
 */
public class ExtentManager {
    static ExtentReports extent;
    static Path path = Paths.get(System.getProperty("user.dir"),"Reports", "FlipPay-API-Automation-Report_"+Helper.getCurrentDateTime()+".html");
    final static String filePath = path.toString();
    
    public synchronized static ExtentReports getReporter() {
        if (Objects.isNull(extent)) {
        	System.out.println("Report invoked !!!!");
            extent = new ExtentReports(filePath, false);
        }
        return extent;
        
    }
}