package qa.automation.com.coposit.Listeners;

import java.util.HashMap;
import java.util.Map;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

/**
 * 
 * Date :- 
 * This is ExtentTestManager Class that has the detailed description about the test execution.
 * @author Basanta
 * @version 1.0
 *
 */
public class ExtentTestManager {  
    static Map<Integer, ExtentTest> extentTestMap = new HashMap<Integer, ExtentTest>();
    static ExtentReports extent = ExtentManager.getReporter();  
    	
    public static synchronized ExtentTest getTest() {

        return (ExtentTest) extentTestMap.get((int) (long) (Thread.currentThread().getId())); 
    }

    public static synchronized void endTest() {
    	System.out.println(">>> Into the endTest <<<<<<<");
        extent.endTest((ExtentTest) extentTestMap.get((int) (long) (Thread.currentThread().getId())));
    }

    public static synchronized ExtentTest startTest(String testName) {
    	System.out.println("Test Started >>> "+testName);
        return startTest(testName, "");
    }
    
    public static synchronized ExtentTest startTest(String testName, String desc) {
        ExtentTest test = extent.startTest(testName, desc);
        extentTestMap.put((int) (long) (Thread.currentThread().getId()), test);
        return test;
    }

    public static synchronized ExtentTest startTest(String testName, String desc, String author_name) {
        ExtentTest test = extent.startTest(testName, desc);
        extentTestMap.put((int) (long) (Thread.currentThread().getId()), test);
        test.assignAuthor(author_name);
        return test;
    }
}