package qa.automation.com.coposit.Listeners;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Method;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import com.relevantcodes.extentreports.LogStatus;

/**
 * 
 * Date :- 13/07/2019
 * For the reporting purpose, we are using Extent Reports. 
 * It generates beautiful HTML reports. We use the extent reports for maintaining logs and also to include the screenshots of failed test cases in the Extent Report.
 * @author Basanta
 * @version 1.0
 *
 */
public abstract class classBase {
   
	/**
	 * 
	 * @param method - This will be invoked before any Test Method
	 */
	@BeforeMethod
    public void beforeMethod(Method method) {
//        ExtentTestManager.startTest(method.getName());
    }
    
	/**d
	 * 
	 * @param result - It will log the result whether test pass, fail or skip
	 */
    @AfterMethod
    protected void afterMethod(ITestResult result)
    {
        if (result.getStatus() == ITestResult.FAILURE) {
            ExtentTestManager.getTest().log(LogStatus.FAIL, result.getThrowable());
        } else if (result.getStatus() == ITestResult.SKIP) {
            ExtentTestManager.getTest().log(LogStatus.SKIP, "Test skipped " + result.getThrowable());
        } else {
            ExtentTestManager.getTest().log(LogStatus.PASS, "Test passed");
        }
        
        ExtentManager.getReporter().endTest(ExtentTestManager.getTest());        
        ExtentManager.getReporter().flush();
    }
    
    /**
     * 
     * @param t - The Throwable class is the superclass of all errors and exceptions
     * @return - It will return the result whether test pass, fail or skip
     */
    protected String getStackTrace(Throwable t) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        t.printStackTrace(pw);
        return sw.toString();
    }
}