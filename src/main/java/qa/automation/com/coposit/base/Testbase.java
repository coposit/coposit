package qa.automation.com.coposit.base;

import java.io.FileInputStream;
import java.util.Properties;

import org.apache.http.client.ClientProtocolException;

public class Testbase {
	
	public int RESPONSE_STATUS_CODE_200 = 200;
	public int RESPONSE_STATUS_CODE_201 = 201;
	public int RESPONSE_STATUS_CODE_303 = 303;
	public int RESPONSE_STATUS_CODE_500 = 500;
	public int RESPONSE_STATUS_CODE_400 = 400;
	public int RESPONSE_STATUS_CODE_401 = 401;
	public int RESPONSE_STATUS_CODE_422 = 422;
	
	public static Properties prop;
	
	public Testbase() {
		try {
			prop = new Properties();
			FileInputStream ip = new FileInputStream("C:\\Users\\ebperals1\\eclipse-workspace\\coposit\\src\\main\\java\\qa\\automation\\com\\coposit\\config\\config.properties");
			prop.load(ip);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String appstarter(String inputurl) throws ClientProtocolException {
		String url = inputurl;
		if(url.equalsIgnoreCase("draftUrl")) {
			return prop.getProperty("draftUrl");
		}
		else if(url.equalsIgnoreCase("uatUrl")) {
			return prop.getProperty("uatUrl");
		}
		else if(url.equalsIgnoreCase("liveUrl")) {
			return prop.getProperty("liveUrl");
		}
		else if(url.equalsIgnoreCase("CMSadmin")) {
			return prop.getProperty("CMSadmin");
		}
		else {
			throw new ClientProtocolException(">>>> Incorrect URL <<<<");
			
		}
	}
}
