package qa.automation.com.coposit.utils;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Random;

import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

/**
 * 
 * Date :- 13/07/2019
 * Utility class (TestUtil.java) stores and handles the functions. The code which is repetitive in nature and which can be commonly used across the entire framework
 * @author Roshan Choudhary
 * @version 1.0
 *
 */
public class TestUtil {
	
	static Xls_Reader reader = new Xls_Reader(System.getProperty("user.dir") + "/TestData/LoginData.xlsx");
	
	public static String getValueByJPath(JSONObject responsejson, String jpath){
		Object obj = responsejson;
		for(String s : jpath.split("/")) 
			if(!s.isEmpty()) 
				if(!(s.contains("[") || s.contains("]")))
					obj = ((JSONObject) obj).get(s);
				else if(s.contains("[") || s.contains("]"))
					obj = ((JSONArray) ((JSONObject) obj).get(s.split("\\[")[0])).get(Integer.parseInt(s.split("\\[")[1].replace("]", "")));
		return obj.toString();
	}
	
	public static int getRandomValue() {
		Random random = new Random();
		int randomNum = random.nextInt(900) +100;
		return randomNum;
	}
	
	public static int getStatusCode(CloseableHttpResponse closeableHttpResponse) {
		System.out.println("Status code --> "+ closeableHttpResponse.getStatusLine().getStatusCode());
		return closeableHttpResponse.getStatusLine().getStatusCode();
	}
	
	public static HashMap<String, String> getAccessTokenInHashmap(String uri) throws ClientProtocolException {
		HashMap<String, String> headerMap = new HashMap<String, String>();
		if (uri.contains("dev")) {
			headerMap.put("Authorization", reader.getCellData("Authorize", 1, 4));
		}
		else if (uri.contains("stage")) {
			headerMap.put("Authorization", reader.getCellData("Authorize", 1, 4));
		}
		else {
			throw new ClientProtocolException();
		}

		return headerMap;
	}
	
	public static void createJsonUsingPOJO(String filename, Object obj) throws JsonGenerationException, JsonMappingException, IOException {
		//jackson api for creating payload
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
		mapper.writeValue(new File(System.getProperty("user.dir")+"/jsonFiles/"+filename), obj);
	}
	
	public static String convertObjectToJsonInString(Object obj) throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
		System.out.println("The JSON String is:\n"+mapper.writerWithDefaultPrettyPrinter().writeValueAsString(obj));
		return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(obj);
	}
	
	public static String getResponseString(CloseableHttpResponse closeableHttpResponse) throws ParseException, IOException {
		return EntityUtils.toString(closeableHttpResponse.getEntity(), "UTF-8");
	}
	
	public static JSONObject convertResponseStringToResponseJSON(String resString) {
		JSONObject responsejson = new JSONObject(resString);
		System.out.println("The response from API is: \n"+ responsejson);
		return responsejson;
	}	

}



