package qa.automation.com.coposit.test;

import org.testng.annotations.Test;

import com.fasterxml.jackson.core.JsonProcessingException;

import static org.testng.Assert.assertEquals;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.Header;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.testng.annotations.BeforeTest;

import qa.automation.com.coposit.DTO.AdminLoginDTO;
import qa.automation.com.coposit.Exception.IncorrectURL;
import qa.automation.com.coposit.Listeners.ExtentTestManager;
import qa.automation.com.coposit.Listeners.classBase;
import qa.automation.com.coposit.base.Testbase;
import qa.automation.com.coposit.clients.RestClient;
import qa.automation.com.coposit.utils.TestUtil;
import qa.automation.com.coposit.utils.Xls_Reader;

public class AdminLoginTest extends classBase {
	
	Testbase testBase;
	String url;	
	RestClient restClient;
	Xls_Reader data;
	AdminLoginDTO adminLogin;
	
	
	@BeforeTest
	public void setup() throws ClientProtocolException, IOException, IncorrectURL {
		testBase = new Testbase();
		try {
			url = testBase.appstarter("CMSadmin") + testBase.prop.getProperty("Loginapi");
		} catch (Exception e) {
			throw new IncorrectURL(">>>> Incorrect URL <<<<");
		}
		System.out.println("Url is : " + url);
		data = new Xls_Reader(System.getProperty("user.dir")+"\\TestData\\LoginData.xlsx");
	}
	
	@Test(priority = 4)
	public void AdminLogin() throws ClientProtocolException, JsonProcessingException, IOException {
		ExtentTestManager.startTest(">>>> Start CMS Admin Login <<<<");
		adminLogin = new AdminLoginDTO("bikash.ebpearls@gmail.com", "pkRZqGy9x");
		restClient = new RestClient();
		CloseableHttpResponse closeableHttpResponse = restClient.postWithoutHeaders(url, TestUtil.convertObjectToJsonInString(adminLogin));
		int responseStatusCode = TestUtil.getStatusCode(closeableHttpResponse);
		
		assertEquals(responseStatusCode, testBase.RESPONSE_STATUS_CODE_200);
		
		Map<String, String> map = new HashMap<>();
		
		Header[] hd = (Header[]) closeableHttpResponse.getAllHeaders();
		for(Header header : hd) {
			map.put(header.getName(), header.getValue());
		}
//		for (String i : map.keySet()) {
//			  System.out.println("key: " + i + " value: " + map.get(i));
//			}
		System.out.println("admin Login access token = " +map.get("accessToken"));
	}

}
