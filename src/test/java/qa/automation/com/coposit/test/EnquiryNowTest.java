package qa.automation.com.coposit.test;

import org.testng.annotations.Test;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import qa.automation.com.coposit.DTO.LoginDTO;
import qa.automation.com.coposit.DTO.MakeEnquiryDTO;
import qa.automation.com.coposit.Exception.IncorrectURL;
import qa.automation.com.coposit.Listeners.ExtentTestManager;
import qa.automation.com.coposit.base.Testbase;
import qa.automation.com.coposit.clients.RestClient;
import qa.automation.com.coposit.utils.TestUtil;
import qa.automation.com.coposit.utils.Xls_Reader;

public class EnquiryNowTest {
	
	Testbase testBase;
	String url;
	LoginDTO loginDto;
	RestClient restClient;
	Xls_Reader data;
	PropertyListTest propertylist;
	MakeEnquiryDTO makeEnquiryDTO;
	
	
	@BeforeTest
	public void setup() throws ClientProtocolException, IOException, IncorrectURL {
		testBase = new Testbase();
		try {
			url = testBase.appstarter("uatUrl") + testBase.prop.getProperty("Enquiryapi");
		} catch (Exception e) {
			throw new IncorrectURL(">>>> Incorrect URL <<<<");
		}
		System.out.println("Url is : " + url);
		data = new Xls_Reader(System.getProperty("user.dir")+"\\TestData\\LoginData.xlsx");
	}
	
	@Test(priority=3)
	public void EnquiryNow() throws ClientProtocolException, IOException, URISyntaxException {
		
		ExtentTestManager.startTest(">>>> Enquiry Now <<<<");
		String getpropertyId = data.getCellData("Sheet1", 0, 1);
		makeEnquiryDTO = new MakeEnquiryDTO(20, 50000, "2", "NSW", "This is the demo enquiry", true, false, 1000, getpropertyId, 104);
		restClient = new RestClient();		
		HashMap<String, String> hashMap = new HashMap<>();
		hashMap.put("Authorization", data.getCellData("Sheet1", 0, 0));
		CloseableHttpResponse closeableHttpResponse = restClient.postWithHeaders(url, TestUtil.convertObjectToJsonInString(makeEnquiryDTO), hashMap);
		int responseStatusCode = TestUtil.getStatusCode(closeableHttpResponse);
		String responseJson = TestUtil.getResponseString(closeableHttpResponse);
		System.out.println("MakeEnquiry Response is >>>>" +responseJson);
		Assert.assertEquals(responseStatusCode, testBase.RESPONSE_STATUS_CODE_200);
	}

}
