package qa.automation.com.coposit.test;

import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.Header;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.impl.client.CloseableHttpClient;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.JsonObject;
import com.relevantcodes.extentreports.LogStatus;

import qa.automation.com.coposit.DTO.LoginDTO;
import qa.automation.com.coposit.Exception.IncorrectURL;
import qa.automation.com.coposit.Listeners.ExtentTestManager;
import qa.automation.com.coposit.Listeners.classBase;
import qa.automation.com.coposit.base.Testbase;
import qa.automation.com.coposit.clients.RestClient;
import qa.automation.com.coposit.utils.TestUtil;
import qa.automation.com.coposit.utils.Xls_Reader;

public class LoginTest extends classBase {
	
	Testbase testBase;
	String url;
	String url1;
	LoginDTO loginDto;
	RestClient restClient;
	Xls_Reader data;
	
	@BeforeTest
	public void setup() throws ClientProtocolException, IOException, IncorrectURL {
		testBase = new Testbase();
		try {
			url = testBase.appstarter("uatUrl") + testBase.prop.getProperty("Loginapi");
			url1 = testBase.appstarter("uatUrl") + testBase.prop.getProperty("propertylist");
		} catch (Exception e) {
			throw new IncorrectURL(">>>> Incorrect URL <<<<");
		}
		System.out.println("Url is : " + url);
		data = new Xls_Reader(System.getProperty("user.dir")+"\\TestData\\LoginData.xlsx");
	}
	
	@Test(priority=1)
	public void Login() throws ClientProtocolException, JsonProcessingException, IOException {
		ExtentTestManager.startTest("Login Test");	
		loginDto = new LoginDTO("Coposit.new@mailinator.com", "Password@123");
		restClient = new RestClient();
		CloseableHttpResponse closeableHttpResponse = restClient.postWithoutHeaders(url, TestUtil.convertObjectToJsonInString(loginDto));
		int responseStatusCode = TestUtil.getStatusCode(closeableHttpResponse);
		AssertJUnit.assertEquals(responseStatusCode, testBase.RESPONSE_STATUS_CODE_200);
		
		Map<String, String> map = new HashMap<String, String>();

		Header[] hM = (Header[]) closeableHttpResponse.getAllHeaders();
		  for (Header header : hM) {
		   map.put(header.getName(), header.getValue());
		  }
		  System.out.println("AcessToken: \n"+map.get("accesstoken"));	
		  data.writeExcelSheet("Sheet1", 0, 0, map.get("accesstoken").toString());
	//	data.setCellData("Sheet1", 0, 0, map.get("accesstoken").toString());
		String reponseJson = TestUtil.getResponseString(closeableHttpResponse);
		ExtentTestManager.getTest().log(LogStatus.INFO, "Response is \n"+reponseJson);
	}
	
//	@Test
//	public void propertylist() throws ClientProtocolException, IOException, URISyntaxException {
//		ExtentTestManager.startTest("Make Enquiry");
//		HashMap<String, String> hashMap = new HashMap<>();
//		hashMap.put("Authorization", data.getCellData("Sheet1", 0, 0));
//		restClient = new RestClient();
//		CloseableHttpResponse closeableHtpResponse = restClient.getWithHeaders(url1, hashMap);
//		int responseStatusCode = TestUtil.getStatusCode(closeableHtpResponse);
//		Assert.assertEquals(responseStatusCode, testBase.RESPONSE_STATUS_CODE_200);
//		JSONObject responseJson = TestUtil.convertResponseStringToResponseJSON(TestUtil.getResponseString(closeableHtpResponse));
//		String propertyid = TestUtil.getValueByJPath(responseJson, "data/docs[0]/_id");
//		System.out.println("PropertyId you are looking for is =" +propertyid);
//	}
	
	
//	@AfterMethod
//	public void tearDown() {
//		
//	}

}
