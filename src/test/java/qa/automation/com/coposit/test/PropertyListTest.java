package qa.automation.com.coposit.test;

import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.json.JSONObject;
import org.testng.annotations.BeforeTest;
import qa.automation.com.coposit.DTO.LoginDTO;
import qa.automation.com.coposit.Exception.IncorrectURL;
import qa.automation.com.coposit.Listeners.ExtentTestManager;
import qa.automation.com.coposit.Listeners.classBase;
import qa.automation.com.coposit.base.Testbase;
import qa.automation.com.coposit.clients.RestClient;
import qa.automation.com.coposit.utils.TestUtil;
import qa.automation.com.coposit.utils.Xls_Reader;

public class PropertyListTest extends classBase {
	
	Testbase testBase;
	String url;
	LoginDTO loginDto;
	RestClient restClient;
	Xls_Reader data;
	
	
	@BeforeTest
	public void setup() throws ClientProtocolException, IOException, IncorrectURL {
		testBase = new Testbase();
		try {
			url = testBase.appstarter("uatUrl") + testBase.prop.getProperty("propertylist");
		} catch (Exception e) {
			throw new IncorrectURL(">>>> Incorrect URL <<<<");
		}
		System.out.println("Url is : " + url);
		data = new Xls_Reader(System.getProperty("user.dir")+"\\TestData\\LoginData.xlsx");
	}
	
	@Test(priority=2)
	public void propertylist() throws ClientProtocolException, IOException, URISyntaxException {
		ExtentTestManager.startTest("Make Enquiry");
		HashMap<String, String> hashMap = new HashMap<>();
		hashMap.put("Authorization", data.getCellData("Sheet1", 0, 0));
		restClient = new RestClient();
		CloseableHttpResponse closeableHtpResponse = restClient.getWithHeaders(url, hashMap);
		int responseStatusCode = TestUtil.getStatusCode(closeableHtpResponse);
		AssertJUnit.assertEquals(responseStatusCode, testBase.RESPONSE_STATUS_CODE_200);
		JSONObject responseJson = TestUtil.convertResponseStringToResponseJSON(TestUtil.getResponseString(closeableHtpResponse));
		String propertyid = TestUtil.getValueByJPath(responseJson, "data/docs[0]/_id");
		System.out.println("PropertyId you are looking for is =" +propertyid);
		data.writeExcelSheet("Sheet1", 0, 1, propertyid);
	}

}
